 def est_croissante(L):
    """
    Teste si la liste L est croissante.
    """

    for i in range(len(L) - 1):
        if not(L[i] <= L[i + 1]):
            return False

    return True

##  ============= Question 1 =============

def insertion_croissante(L, x):
    """
    L est une liste de flottants qui est triée par ordre croissant et x est un flottant.
    Ajoute l'élément x à la liste L en l'insérant à sa place.
    """
    pos = 0
    n = len(L)
    
    while pos < n and L[pos] < x:
        pos += 1
    
    L.insert(pos , x)



# Tests question 1

L1 = [-1.2, 1, 2]
insertion_croissante(L1, 0.5)   # vérifie l'insertion sur un exemple générique
print(L1)
L2 = [-1.2, 1]
insertion_croissante(L2, 1.5)   # vérifie l'insertion à la fin
print(L2)
L3 = [-1.2, 1, ]
insertion_croissante(L3, -2.5)  # vérifie l'insertion au début
print(L3)
L4 = []
insertion_croissante(L4, 0.5)   # vérifie l'insertion sur la liste vide
print(L4)

##  ============= Question 2 ============

def k_plus_petits(L, k):
    """
    Renvoie la liste des k plus petis éléments de L
    """

    assert type(k) == int and k > 0, "k doit être un entier strictement positif"
    assert k <= len(L), "La liste ne contient pas assez d'éléments"
    
    plus_petits = []

    for i in range(k):
        insertion_croissante(plus_petits, L[i])

    for i in range(k, len(L)):
        x = L[i]
        if x < plus_petits[-1]:
            plus_petits.pop()
            insertion_croissante(plus_petits, x)

    return plus_petits

# Test question 2

from random import randint
L_alea = [randint(1,100) for k in range(20)]
print(L_alea)   # Affiche la liste aléatoire L  
print(k_plus_petits(L_alea, 4))    # Affiche les 4 plus petits éléments de L

L1 = [2, -2, 1.2]
print(k_plus_petits(L1, 1))
L2 = [2.5, -3, 1.2]
print(k_plus_petits(L2, 3))

# -------------  Importation des données dans BDIris

import pandas as pd
iris = pd.read_csv("iris.csv")
LONGUEURS_sep = iris.loc[:, "sepal_length"]
LARGEURS_sep = iris.loc[:, "sepal_width"]
LONGUEURS_pet = iris.loc[:,"petal_length"]
LARGEURS_pet = iris.loc[:,"petal_width"]
ESPECES = iris.loc[:,"species"]
BDIris = []
for i in range(150):
    (long_sep, larg_sep, long_pet, larg_pet) = (LONGUEURS_sep[i], LARGEURS_sep[i], 
                                                LONGUEURS_pet[i], LARGEURS_pet[i])
    if ESPECES[i] == 'setosa':
        esp = 0
    elif ESPECES[i] == 'virginica':
        esp = 1
    elif ESPECES[i] == 'versicolor':
        esp = 2
        
    BDIris.append((long_sep, larg_sep, long_pet, larg_pet, esp))

# ---------   Fonction distance

from math import sqrt
def distance(fleur_1, fleur_2):
    """Renvoie la distance entre les deux fleurs."""
    
    (x_1, y_1, z_1, t_1, esp1) = fleur_1
    (x_2, y_2, z_2, t_2, esp2) = fleur_2
    return sqrt((x_2 - x_1)**2 + (y_2 - y_1)**2 + (z_2 - z_1)**2 + (t_2 - t_1)**2)

#  ===============  Question 3  ==============

def insertion_fleur(nouv_fleur, L, fleur):
    """
    nouv_fleur est la fleur de référence. L est une liste de fleurs qui est triée 
    par ordre croissant des distances à nouv_fleur.
    Ajoute fleur à la liste L en l'insérant à sa place pour la distance à nouv_fleur.
    """
    
    pos = 0
    n = len(L)
    
    while pos < n and distance(L[pos], nouv_fleur) < distance(fleur, nouv_fleur):
        pos += 1
    
    L.insert(pos , fleur)

#  Test question 3

f_b = (5, 3, 0, 0, 1)
f_1 = (6, 1, 0, 0, 0)
f_2 = (2, 3, 0, 0, 0)
f_3 = (3, 6, 0, 0, 0)
f_4 = (1, 5, 0, 0, 2)  # f_1, .. f_4 sont 4 fleurs placées à distance croissante de f_b
L1 = [f_1, f_3, f_4]; insertion_fleur(f_b, L1, f_2); 
print(L1)  # vérifie l'insertion sur un exemple générique
L2 = [f_1, f_2]; insertion_fleur(f_b, L2, f_4)
print(L2)   # vérifie l'insertion à la fin
L3 = [f_3, f_4]; insertion_fleur(f_b, L3, f_2)
print(L3)   # vérifie l'insertion au début
L4 = []; insertion_fleur(f_b, L4, f_2)
print(L4)   # vérifie l'insertion sur la liste vide

#  ===============  Question 4  ==============

def k_NN(nouv_fleur, k):
    """
    Renvoie la liste de k fleurs les plus proches de nouv_fleur.
    """
    
    assert type(k) == int and 0 < k < 150, "k doit être un entier compris entre 1 et 150"
    
    plus_proches = []
    
    for i in range(k):
        insertion_fleur(nouv_fleur, plus_proches, BDIris[i])

    for i in range(k, len(BDIris)):
        if distance(nouv_fleur, BDIris[i]) < distance(nouv_fleur, plus_proches[-1]):
            plus_proches.pop()
            insertion_fleur(nouv_fleur, plus_proches, BDIris[i])

    return plus_proches

# Test question 4

k_NN((5,3,2,0.5,-1), 3) 
# Doit renvoyer [(5.1, 3.3, 1.7, 0.5, 0), (5.0, 3.0, 1.6, 0.2, 0), (4.8, 3.4, 1.9, 0.2, 0)]

#  ===============  Question 5  ==============

def indice_max(L):
    """
    L est une liste non vide d'entiers. Renvoie l'indice de la première occurence 
    du maximum de la liste.
    """
    
    i_max = 0
    
    for i in range(1, len(L)):
        if L[i] > L[i_max]:
            i_max = i
            
    return i_max

def prediction_espece(nouv_fleur, k):
    """
    Renvoie le code de l'espèce que la méthode des k plus proches voisins attribue 
    à nouv_fleur d'après les données de BDIris. Dans le cas d'égalité, n'importe 
    laquelle des réponses possibles est bonne.
    """
    
    plus_proches = k_NN(nouv_fleur, k)
    compteurs = [0,0,0]  # compteurs[i] va compter les fleurs d'espèce codée par i.

    for fleur in plus_proches:
        (_,_,_,_,esp) = fleur
        compteurs[esp] += 1

    return indice_max(compteurs)

# Test question 5

print(prediction_espece((5,3,2,0.5,-1), 5))  # doit renvoyer 0
print(prediction_espece((5,3,5,2,-1), 5))    # doit renvoyer 1
print(prediction_espece((5,3,3,1,-1), 5))    # doit renvoyer 2


    
